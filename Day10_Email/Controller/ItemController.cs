﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Day10_Email.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class itemController : ControllerBase
    {
        [Authorize]
        [HttpGet]
        [Route("Hello")]

        public string hello()
        {
            return "Hello";
        }
    }
}
